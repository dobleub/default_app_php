<?php namespace App\Models;

use CodeIgniter\Model;

class PeopleModel extends Model
{
    protected $table      = 'people';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['alias', 'name', 'lastname', 'phone', 'avatar', 'address', 'token', 'status', 'id_user'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}