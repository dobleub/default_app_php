<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */
// routes
$routes->addPlaceholder('uuid', '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');
// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

// Signin
$routes->add('get_signin_modal', 'Signin::get_signin_modal', ['filter' => 'no_auth']);
$routes->add('get_signin_user', 'Signin::get_signin_user', ['filter' => 'no_auth']);
// Signup
$routes->add('get_signup_modal', 'Signin::get_signup_modal', ['filter' => 'no_auth']);
$routes->add('save_signup_user', 'Signin::save_signup_user', ['filter' => 'no_auth']);
// Forgot password
$routes->add('get_forgot_password_modal', 'Signin::get_forgot_password_modal', ['filter' => 'no_auth']);
$routes->add('send_forgot_password_mail', 'Signin::send_forgot_password_mail', ['filter' => 'no_auth']);
$routes->add('reset/password/(:uuid)', 'Signin::reset_password/$1', ['filter' => 'no_auth']);
$routes->add('save_new_password', 'Signin::save_new_password', ['filter' => 'no_auth']);
// Logout
$routes->add('logout_user', 'Signin::logout_user');


// User
$routes->group('user', ['filter' => 'auth'], function($routes) {
	$routes->add('/', 'User\Home');
	$routes->add('people/update', 'User\Home::update_people_information');
	
	// Users
	$routes->group('users', function($routes) {
		$routes->get('get/(:uuid)', 'Signin::get_user/$1');
	});
});


// Admin
$routes->group('admin', ['filter' => 'admin_auth'], function($routes) {
	$routes->add('/', 'Admin\Home');
	
});


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
