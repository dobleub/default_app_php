<?php namespace App\Controllers;

use App\Helpers\Constants;

class Home extends BaseController
{
	public function index()
	{
		$data = [
			'title' => 'Home - '.$this->appName(),
			'name' => $this->session->get('name'),
			'email' => $this->session->get('email'),
			'token' => $this->session->get('token'),
			'isAdmin' => $this->session->get('role') == Constants::ADMIN_ROLE ? true : false
		];
		
		return view('home', $data);
	}

	//--------------------------------------------------------------------

}
