<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\PeopleModel;
use App\Models\RoleModel;
use App\Models\PermissionModel;
use App\Helpers\Constants;

class Signin extends BaseController
{
	public function index() {
		return view('login');
	}

	/**
	 * Save user session
	 */
	private function setUserSession($user, $people) {
		$roleModel = new RoleModel();
		$permissionModel = new PermissionModel();

		$role = $roleModel->find( $user['id_role'] );
		$permissions = $permissionModel->where('id_role', $user['id_role']);

		$data = [
			'id' => $user['id'],
			'name' => $people['name'],
			'email' => $user['email'],
			'token' => $user['token'],
			'role' => $role['name'],
			'avatar' => $people['avatar'],
			'permissions' => $permissions,
			'isLoggedIn' => true,
		];
		
		$this->session->set($data);
		return true;
	}

	/**
	 * Login 
	 */
	public function get_signin_modal() {
		return view('signin/signin');
	}
	public function get_signin_user() {
		helper(['form', 'url']);
		$response = ['status'=>'error', 'data'=>[], 'msg'=>'Undefined', 'details'=>[]];
		$userModel = new UserModel();
		$peopleModel = new PeopleModel();
         
        $val = $this->validate([
            'email' => 'required|valid_email',
            'password'  => 'required'
		]);
		
        if ($val) {
			$user = $userModel->where('email', $this->request->getVar('email'))->first();

			if ($user) {
				$valid = password_verify($this->request->getVar('password'), $user['password']);
			}

			if ($user && $valid) {
				$people = $peopleModel->where('id_user', $user['id'])->first();

				$response['status'] = 'ok';
				$response['data'] = ['name' => $people['name'], 'email' => $user['email']];
				$this->setUserSession($user, $people);
			} else {
				$response['status'] = 'error';
				$response['msg'] = 'User not found';
				$response['details'] = [];
			}
		} else {
			$response['status'] = 'error';
			$response['msg'] = 'Validation error';
			$response['details'] = $this->validator->getErrors();
		}
		
		return json_encode($response);
	}
	
	/**
	 * Register 
	 */
	public function get_signup_modal() {
		return view('signin/signup');
	}
	public function save_signup_user() {
		helper(['form', 'url']);
		$response = ['status'=>'error', 'msg'=>'Undefined', 'details'=>[]];
		$roleModel = new RoleModel();
		$userModel = new UserModel();
		$peopleModel = new PeopleModel();
        
        $val = $this->validate([
            'name' => 'required',
            'email' => 'required|valid_email',
            'password'  => 'required',
            'agree'  => 'required'
		]);

        if ($val) {
        	$role = $roleModel->where('name', Constants::CLIENT_ROLE)->first();
			$userData = [
                'email'  => $this->request->getVar('email'),
				'password'  => password_hash($this->request->getVar('password'), PASSWORD_ARGON2I),
				'token' => $this->get_uuid(),
				'id_role' => $role['id']
			];
			
			$user = $userModel->where('email', $userData['email'])->first();
			
			if ($this->request->getVar('agree') == '1' && !$user) {

				try {
					$saved = $userModel->save($userData);
					$user = $userModel->find( $userModel->insertID() );
					
					$peopleData = [
	                	'name' => $this->request->getVar('name'),
	                	'id_user' => $user['id']
					];

					$saved = $peopleModel->save($peopleData);
					$people = $peopleModel->find( $peopleModel->insertID() );

					$this->setUserSession($user, $people);
					
					$response['status'] = 'ok';
					$response['msg'] = 'Done';
					$response['data'] = ['name' => $people['name'], 'email' => $user['email'], 'role' => $role['name']];
				} catch (Exception $e) {
					$response['status'] = 'error';
					$response['msg'] = 'Not saved';
					$response['details'] = $e->getMessage();
				}	
			} else {
				if ($user) {
					$response['status'] = 'error';
					$response['msg'] = 'User email is in use';
				} else {
					$response['status'] = 'error';
					$response['msg'] = 'Please agree the terms and policy';
				}
			}
		} else {
			$response['status'] = 'error';
			$response['msg'] = 'Validation error';
			$response['details'] = $this->validator->getErrors();
		}
		
		return json_encode($response);
	}

	/**
	 * Get user by token
	 */
	public static function get_user_by_token($token) {
		$response = ['status'=>'error', 'data'=>[], 'msg'=>'Undefined', 'details'=>[]];
		$roleModel = new RoleModel();
		$userModel = new UserModel();
		$peopleModel = new PeopleModel();
		$permissionModel = new PermissionModel();
        
        if ($token) {
			$user = $userModel->where('token', $token)->first();
			
			if ($user) {
				$people = $peopleModel->where('id_user', $user['id'])->first();
				$role = $roleModel->find( $user['id_role'] );
				$permissions = $permissionModel->where('id_role', $user['id_role']);

				$response['status'] = 'ok';
				$response['data'] = ['user' => $user, 'people' => $people, 'role' => $role, 'permissions' => $permissions];
			} else {
				$response['status'] = 'error';
				$response['msg'] = 'User not found';
				$response['details'] = [];
			}
		} else {
			$response['status'] = 'error';
			$response['msg'] = 'Validation error';
			$response['details'] = $this->validator->getErrors();
		}
		
		return $response;
	}
	
	/**
	 * Forgot password 
	 */
	public function get_forgot_password_modal() {
		return view('signin/forgot_password');
	}
	public function send_forgot_password_mail() {
		helper(['form', 'url']);
		$response = ['status'=>'error', 'msg'=>'Undefined', 'details'=>[]];
		$userModel = new UserModel();

		$val = $this->validate([
			'email' => 'required|valid_email'
		]);
		
		if ($val) {
			$user = $userModel->where('email', $this->request->getVar('email'))->first();

			$data = [
				'name' => $user['name'],
				'url_token' => base_url('reset/password/'.$user['token']),
			];

			if ($user) {
				$email = \Config\Services::email();
				$email->setNewLine("\r\n");

				$email->setFrom('info@equidiet.com', 'Equidiet');
				$email->setTo($user['email'], $user['name']);
				// $email->setBCC('osorio.edd@gmail.com');

				$email->setSubject('Equidiet - Forgot password?');

				$body = view('emails/forgot_password', $data);
				$email->setMessage($body);

				if ($email->send()) {
					$response['status'] = 'ok';
					$response['data'] = [];
				} else {
					$response['status'] = 'error';
					$response['msg'] = 'Email not sended';
					$response['details'] = $email->send();
				}
			} else {
				$response['status'] = 'error';
				$response['msg'] = 'User not found';
				$response['details'] = [];
			}
		} else {
			$response['status'] = 'error';
			$response['msg'] = 'Validation error';
			$response['details'] = $this->validator->getErrors();
		}

		return json_encode($response);
	}
	public function reset_password($token) {
		helper(['url']);
		$response = ['status'=>'error', 'msg'=>'Undefined', 'details'=>[]];
		$userModel = new UserModel();
		$userData = [];

		if ($token) {
			$userData = Signin::get_user_by_token($token);
		}

		return view('signin/reset_password', ['token' => $token]);
	}
	public function save_new_password() {
		helper(['form', 'url']);
		$response = ['status'=>'error', 'data'=>[], 'msg'=>'Undefined', 'details'=>[]];
		$userModel = new UserModel();
		$peopleModel = new PeopleModel();

		$val = $this->validate([
			'token' => 'required',
			'password' => 'required',
        	'confirm_password' => 'required|matches[password]'
		]);

		if ($val) {
			$user = $userModel->where('token', $this->request->getVar('token'))->first();

			if ($user) {
				try {
					$user['password'] = password_hash($this->request->getVar('password'), PASSWORD_ARGON2I);
					$userModel->save($user);

					$people = $peopleModel->where('id_user', $user['id'] )->first();

					$this->setUserSession($user, $people);

					$response['status'] = 'ok';
					$response['msg'] = 'Done';
					$response['data'] = $user;
				} catch (Exception $e) {
					$response['status'] = 'error';
					$response['msg'] = 'Not saved';
					$response['details'] = $e->getMessage();
				}	
			} else {
				$response['status'] = 'error';
				$response['msg'] = 'Password cannot be saved';
			}
		} else {
			$response['status'] = 'error';
			$response['msg'] = 'Password should completely match';
			$response['details'] = $this->validator->getErrors();
		}

		return json_encode($response);
	}

	/**
	 * Logout 
	 */
	public function logout_user() {
		$this->session->destroy();

		return redirect()->to('/');
	}
}
