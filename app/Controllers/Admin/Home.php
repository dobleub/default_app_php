<?php namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\UserModel;
use CodeIgniter\I18n\Time;

class Home extends BaseController
{	
	public function index()
	{
		$userModel = new UserModel();

		$users = $userModel->findAll();
		$data = [
			'title' => 'Admin - '.$this->appName(),
		];
		return view('admin/home', $data);
	}

	//--------------------------------------------------------------------

}
