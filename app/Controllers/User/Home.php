<?php namespace App\Controllers\User;

use App\Models\PeopleModel;
use App\Controllers\BaseController;
use App\Controllers\Signin;
use CodeIgniter\I18n\Time;

class Home extends BaseController
{	
	public function index()
	{
		$user = Signin::get_user_by_token( $this->session->get('token') );

		$data = [
			'title' => 'User - '.$this->appName(),
			'people' => $user['data']['people'],
			'user' => $user['data']['user']
		];

		return view('user/home', $data);
	}

	/**
	 * Update people
	 */
	public function update_people_information() {
		helper(['form', 'url']);
		$response = ['status'=>'error', 'data'=>[], 'msg'=>'Undefined', 'details'=>[]];
		$peopleModel = new PeopleModel();

		$val = $this->validate([
			'name' => 'required',
		]);

		// $files = $this->request->getFiles();
		$name = $this->request->getVar('name');
		$phone = $this->request->getVar('phone');
		$address = $this->request->getVar('address');
		$file = $this->request->getFile('avatar');

		if ($val) {
			$people = $peopleModel->where('id_user', $this->session->get('id'))->first();

			if ($people) {
				try {
					$people['name'] = isset($name) && $name != NULL ? $name : 'User name';
					$people['phone'] = isset($phone) && $phone != NULL ? $phone : '';
					$people['address'] = isset($address) && $address != NULL ? $address : '';
					if (isset($file) && $file != NULL) {
						if ($file->isValid() && !$file->hasMoved()) {
							$newName = $file->getRandomName();
							$file->move('static/avatars', $newName);
							$people['avatar'] = $newName;
						}
					}

					$peopleModel->save($people);

					$response['status'] = 'ok';
					$response['msg'] = 'Done';
					$response['data'] = ['name' => $people['name'], 'phone' => $people['phone'], 'address' => $people['address']];
				} catch (Exception $e) {
					$response['status'] = 'error';
					$response['msg'] = 'Not saved';
					$response['details'] = $e->getMessage();
				}	
			} else {
				$response['status'] = 'error';
				$response['msg'] = 'Password cannot be saved';
			}
		} else {
			$response['status'] = 'error';
			$response['msg'] = 'Invalid data, please try again';
			$response['details'] = $this->validator->getErrors();
		}

		return json_encode($response);
	}

}
