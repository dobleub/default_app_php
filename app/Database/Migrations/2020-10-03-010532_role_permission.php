<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RolePermission extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id_role'	 => [
                'type'			=> 'INT',
                'unsigned'		=> true,
            ],
            'id_permission'	 => [
                'type'			=> 'INT',
                'unsigned'		=> true,
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'deleted_at DATETIME DEFAULT NULL'
        ]);
        $this->forge->addKey('id_role', true);
        $this->forge->addKey('id_permission', true);
        $this->forge->addField('CONSTRAINT FOREIGN KEY (id_role) REFERENCES role(id)');
        $this->forge->addField('CONSTRAINT FOREIGN KEY (id_permission) REFERENCES permission(id)');
        
        $attributes = ['ENGINE' => 'InnoDB'];
        $this->forge->createTable('role_permission', TRUE, $attributes);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('role_permission', TRUE);
	}
}
