<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class People extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id'	 => [
                'type'			=> 'INT',
                'unsigned'		=> true,
                'auto_increment' => true,
            ],
            'alias'	 => [
                'type'			=> 'VARCHAR',
                'constraint'	=> '50',
                'null'			=> true,
            ],
            'name'	 => [
                'type'			=> 'VARCHAR',
                'constraint'	=> '255',
            ],
            'lastname'  => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
                'null'          => true,
            ],
            'gender'   => [
                'type'          => 'VARCHAR',
                'constraint'    => '25',
                'null'			=> true,
            ],
            'phone'   => [
                'type'          => 'VARCHAR',
                'constraint'    => '15',
                'null'          => true,
            ],
            'avatar'   => [
                'type'          => 'TEXT',
                'null'			=> true,
            ],
            'address'   => [
                'type'          => 'TEXT',
                'null'			=> true,
            ],
            'token'  => [
                'type'          => 'VARCHAR',
                'constraint'    => '64',
            ],
            'status' => [
                'type'          => 'TINYINT',
                'constraint'    => '1',
                'default'       => 1
            ],
            'id_user'   => [
                'type'          => 'INT',
                'unsigned'      => true,
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'deleted_at DATETIME DEFAULT NULL'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addField('CONSTRAINT FOREIGN KEY (id_user) REFERENCES user(id)');
        
        $attributes = ['ENGINE' => 'InnoDB'];
        $this->forge->createTable('people', TRUE, $attributes);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('people', TRUE);
	}
}
