<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Permission extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id'	 => [
                'type'			=> 'INT',
                'unsigned'		=> true,
                'auto_increment' => true,
            ],
            'name'	 => [
                'type'			=> 'VARCHAR',
                'unique'        => true,
                'constraint'	=> '50',
            ],
            'token'  => [
                'type'          => 'VARCHAR',
                'constraint'    => '64',
            ],
            'status' => [
                'type'          => 'TINYINT',
                'constraint'    => '1',
                'default'       => 1
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'deleted_at DATETIME DEFAULT NULL'
        ]);
        $this->forge->addKey('id', true);

        $attributes = ['ENGINE' => 'InnoDB'];
        $this->forge->createTable('permission', TRUE, $attributes);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('permission', TRUE);
	}
}
