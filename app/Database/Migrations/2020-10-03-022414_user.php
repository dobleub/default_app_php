<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
	public function up()
    {
        $this->forge->addField([
            'id'	 => [
                'type'			=> 'INT',
                'unsigned'		=> true,
                'auto_increment' => true,
            ],
            'email'	 => [
                'type'			=> 'VARCHAR',
                'unique'        => true,
                'constraint'	=> '150',
            ],
            'password'  => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
            ],
            'token'  => [
                'type'          => 'VARCHAR',
                'constraint'    => '64',
            ],
            'status' => [
                'type'          => 'TINYINT',
                'constraint'    => '1',
                'default'       => 1
            ],
            'id_role'   => [
                'type'          => 'INT',
                'unsigned'      => true,
            ],
            'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'deleted_at DATETIME DEFAULT NULL'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addField('CONSTRAINT FOREIGN KEY (id_role) REFERENCES role(id)');
        
        $attributes = ['ENGINE' => 'InnoDB'];
        $this->forge->createTable('user', TRUE, $attributes);
    }

    public function down()
    {
        $this->forge->dropTable('user', TRUE);
    }
}
