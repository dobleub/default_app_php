<?php namespace App\Database\Seeds;

use App\Libraries\Uuid;

class RoleSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $uuid = new Uuid();

        $data = [
            [
                'name' => 'admin',
                'description' => 'Administrador del sistema',
                'token' => $uuid->v4()
            ],
            [
                'name' => 'editor',
                'description' => 'Editor del sistema',
                'token' => $uuid->v4()
            ],
            [
                'name' => 'client',
                'description' => 'Cliente',
                'token' => $uuid->v4()
            ],
            [
                'name' => 'auditor',
                'description' => 'Auditor del sistema',
                'token' => $uuid->v4()
            ],
        ];

        // Simple Queries
        // $this->db->query("INSERT INTO role(name, email, token) VALUES(:name, :email, :token)", $data);
        
        // Using Query Builder
        $this->db->table('role')->insertBatch($data);
    }
}