<?php namespace App\Database\Seeds;

use App\Libraries\Uuid;

class PermissionSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $uuid = new Uuid();

        $data = [
            [
                'name' => 'settings',
                'token' => $uuid->v4()
            ],
            [
                'name' => 'admin',
                'token' => $uuid->v4()
            ],
            [
                'name' => 'dashboard',
                'token' => $uuid->v4()
            ],
        ];

        // Using Query Builder
        $this->db->table('permission')->insertBatch($data);
    }
}