<?php namespace App\Database\Seeds;

use App\Libraries\Uuid;

class RolePermissionSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $uuid = new Uuid();

        $rolesPermission = [];
        $roles = $this->db->query("SELECT * FROM role");
        $permissions = $this->db->query("SELECT * FROM permission");

        foreach ($roles->getResult() as $rol) {
            foreach ($permissions->getResult() as $per) {
                if ($rol->name == 'admin') {
                    $rolesPermission[] = ['id_role' => $rol->id, 'id_permission' => $per->id];
                }
                if ($rol->name == 'editor' && in_array($per->name, ['settings', 'admin'])) {
                    $rolesPermission[] = ['id_role' => $rol->id, 'id_permission' => $per->id];
                }
                if ($rol->name == 'client' && $per->name == 'settings') {
                    $rolesPermission[] = ['id_role' => $rol->id, 'id_permission' => $per->id];
                }
                if ($rol->name == 'auditor' && $per->name == 'dashboard') {
                    $rolesPermission[] = ['id_role' => $rol->id, 'id_permission' => $per->id];
                }
            }
        }

        if (count($rolesPermission) > 0) {
            $this->db->table('role_permission')->insertBatch($rolesPermission);
        }
    }
}