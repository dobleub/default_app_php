<?php namespace App\Helpers;

/**
 * Collection of definitions
 */
class Constants {
	/**
	 * Roles
	 */
	const AUDITOR_ROLE	= 'auditor';
	const CLIENT_ROLE	= 'client';
	const EDITOR_ROLE 	= 'editor';
	const ADMIN_ROLE 	= 'admin';
}