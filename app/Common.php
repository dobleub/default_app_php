<?php

/**
 * The goal of this file is to allow developers a location
 * where they can overwrite core procedural functions and
 * replace them with their own. This file is loaded during
 * the bootstrap process and is called during the frameworks
 * execution.
 *
 * This can be looked at as a `master helper` file that is
 * loaded early on, and may also contain additional functions
 * that you'd like to use throughout your entire application
 *
 * @link: https://codeigniter4.github.io/CodeIgniter4/
 */

if ($_ENV['CI_ENVIRONMENT'] === 'development') {	
	$useKint = true;
}

$protocolRaw = $_SERVER['SERVER_PROTOCOL'];
$protocol = explode('/', $protocolRaw);
$url = strtolower($protocol[0]) . '://' . $_SERVER['HTTP_HOST'];
$parsedUrl = parse_url($url);
$host = explode('.', $parsedUrl['host']);
// This will create a config variable accessible like other config vars
define('CI_SUBDOMAIN', $host[0]);
