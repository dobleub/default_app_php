<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

use App\Controllers\Signin;
use App\Helpers\Constants;

class AdminAuth implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $auth = service('auth');
        $this->session = \Config\Services::session();
        $role = null;
        $token = $this->session->get('token') ?? null;
        $isLoggedIn = $this->session->get('isLoggedIn') ?? false;

        if ($isLoggedIn && $token) {
            $data = Signin::get_user_by_token($token);
            
            if (count($data['data']) > 0) {
                $data = [
                    'id' => $data['data']['user']['id'],
                    'name' => $data['data']['people']['name'],
                    'email' => $data['data']['user']['email'],
                    'token' => $data['data']['user']['token'],
                    'role' => $data['data']['role']['name'],
                    'avatar' => $data['data']['people']['avatar'],
                    'permissions' => $data['data']['permissions'],
                    'isLoggedIn' => true,
                ];
                
                $this->session->set($data);
            }

            $role = $this->session->get('role');
            $token = $this->session->get('token');
            $isLoggedIn = $this->session->get('isLoggedIn') ?? false;
        }

        if (!$isLoggedIn || !isset($role) || $role === null || $role !== Constants::ADMIN_ROLE) {
            return redirect()->to('/');
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}