<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class NoAuth implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $auth = service('auth');
        $this->session = \Config\Services::session();
        $role = $this->session->get('role') ?? null;
        $isLoggedIn = $this->session->get('isLoggedIn') ?? false;

        if ($isLoggedIn) {
			return redirect()->to('/');
		}
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}