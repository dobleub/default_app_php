<div class="modal-content form-holder has-shadow">
  <div class="row">
    <!-- Logo & Information Panel-->
    <div class="col-lg-6">
      <div class="info d-flex align-items-center">
        <div class="content">
          <div class="logo">
            <h1>Forgot password?</h1>
          </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
      </div>
    </div>
    <!-- Form Panel    -->
    <div class="col-lg-6 bg-white">
      <div class="form d-flex align-items-center">
        <div class="content">
          <form class="form-validate" action=<?= base_url('send_forgot_password_mail') ?> method="post">
            <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
            <div class="form-group">
              <input id="register-email" type="email" name="email" required data-msg="Please enter a valid email address" class="input-material">
              <label for="register-email" class="label-material">Email Address</label>
            </div>
            <div class="form-group">
              <button id="regidter" type="submit" name="forgotSubmit" class="btn btn-primary">Send email</button>
            </div>
          </form>
          <small>Already have an account? </small>
          <a href="login.html" class="signup" onclick="get_signin_modal(event, 'signin');">Login</a>
        </div>
      </div>
    </div>
  </div>
</div>