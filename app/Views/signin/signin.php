<div class="modal-content form-holder has-shadow">
    <div class="row">
    
    <!-- Logo & Information Panel-->
    <div class="col-lg-6">
        <div class="info d-flex align-items-center">
        <div class="content">
            <div class="logo">
            <h1>Sign in</h1>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
        </div>
    </div>
    <!-- Form Panel    -->
    <div class="col-lg-6 bg-white">
        <div class="form d-flex align-items-center">
        <div class="content">
            <form class="form-validate" action=<?= base_url('get_signin_user') ?> method="post">
                <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
                <div class="form-group">
                    <input id="register-email" type="email" name="email" required data-msg="Please enter a valid email address" class="input-material">
                    <label for="register-email" class="label-material">Email Address</label>
                </div>
                <div class="form-group">
                    <input id="login-password" type="password" name="password" required data-msg="Please enter your password" class="input-material">
                    <label for="login-password" class="label-material">Password</label>
                </div>
                <div class="form-group">
                    <button id="login" type="submit" name="login" class="btn btn-primary">Login</button>
                </div>
            </form>
            <a href="#" class="forgot-pass" onclick="get_signin_modal(event, 'forgot_password');">Forgot Password?</a>
            <br><small>Do not have an account? </small>
            <a href="register.html" class="signup" onclick="get_signin_modal(event, 'signup');">Signup</a>
        </div>
        </div>
    </div>

    </div>
</div>
