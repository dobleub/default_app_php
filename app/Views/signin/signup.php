<div class="modal-content form-holder has-shadow">
  <div class="row">
    
    <!-- Logo & Information Panel-->
    <div class="col-lg-6">
      <div class="info d-flex align-items-center">
        <div class="content">
          <div class="logo">
            <h1>Sign up</h1>
          </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        </div>
      </div>
    </div>
    <!-- Form Panel    -->
    <div class="col-lg-6 bg-white">
      <div class="form d-flex align-items-center">
        <div class="content">
          <form class="form-validate" action="<?= base_url('save_signup_user') ?>" method="post">
            <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
            <div class="form-group">
              <input id="register-username" type="text" name="name" required data-msg="Please enter your name" class="input-material">
              <label for="register-username" class="label-material">Name</label>
            </div>
            <div class="form-group">
              <input id="register-email" type="email" name="email" required data-msg="Please enter a valid email address" class="input-material">
              <label for="register-email" class="label-material">Email Address</label>
            </div>
            <div class="form-group">
              <input id="register-password" type="password" name="password" required data-msg="Please enter your password" class="input-material">
              <label for="register-password" class="label-material">Password</label>
            </div>
            <div class="form-group terms-conditions">
              <input id="register-agree" name="agree" type="checkbox" required value="1" data-msg="Your agreement is required" class="checkbox-template">
              <label for="register-agree">Agree the terms and policy</label>
            </div>
            <div class="form-group">
              <button id="regidter" type="submit" name="register" class="btn btn-primary">Sign up</button>
            </div>
          </form>
          <small>Already have an account? </small>
          <a href="login.html" class="signup" onclick="get_signin_modal(event, 'signin');">Login</a>
        </div>
      </div>
    </div>

  </div>
</div>