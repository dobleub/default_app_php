<?= $this->extend('templates/simple') ?> 

<?= $this->section('content') ?>

      <div id="content">
        
        <div class="container">
          <div class="row">
            <div class="offset-lg-3 col-lg-6 bg-white">
              <div class="box">
                <h2 class="text-uppercase">Reset your password</h2>
                <p class="lead">Not our registered customer yet?</p>
                <p>With registration with us new world of fashion, fantastic discounts and much more opens to you! The whole process will not take you more than a minute!</p>
                <p class="text-muted">If you have any questions, please feel free to <a href="contact.html">contact us</a>, our customer service center is working for you 24/7.</p>
                <br>
                <div class="form align-items-center">
                  <div class="content">
                      <form class="form-validate" action=<?= base_url('save_new_password') ?> method="post">
                          <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
                          <input type="hidden" name="token" value="<?= $token ?>" />
                          <div class="form-group-material">
                            <input id="name-login" type="password" name="password" required data-msg="Please enter your password" class="input-material">
                            <label for="name-login" class="label-material">New password</label>
                          </div>
                          <div class="form-group-material">
                            <input id="email-login" type="password" name="confirm_password" required data-msg="Please confirm your password" class="input-material">
                            <label for="email-login" class="label-material">Confirm password</label>
                          </div>
                          <div class="form-group">
                            <button id="reset_password" type="submit" name="reset_password" class="btn btn-primary">Reset password</button>
                          </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

<?= $this->endSection() ?>