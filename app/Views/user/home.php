<?= $this->extend('templates/admin') ?> 

<?= $this->section('content') ?>

<!-- Page Header-->
<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">User</h2>
  </div>
</header>
<!-- Dashboard Counts Section-->
<section class="forms no-padding-bottom">
  <div class="container-fluid">
    <div class="row">

      <!-- Form-->
      <div class="col-lg-6">
        <div id="update-personal-information" class="form card">
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Personal information</h3>
          </div>
          
          <form class="form-validate" action="<?= base_url('user/people/update') ?>" method="post">
            <div class="card-body">
              <p>Basic personal information</p>
                <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
                <div class="row align-items-center mb-5">
                  <div class="col-5">
                    <div class="avatar text-center">
                      <img id="preview-update-avatar" src="<?= $people['avatar']!=NULL || $people['avatar']!='' ? '/static/avatars/'.$people['avatar'] : '/public/admin/img/avatar.png' ?>" alt="Avatar" class="img-fluid rounded-circle">
                    </div>
                    <div id="progress-wrp">
                      <div role="progressbar" style="width: 0%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
                    </div>
                  </div>
                  <div class="col-7 justify-content-center">
                    <input id="update-avatar" type="file" name="avatar" data-msg="Please upload your avatar" class="form-control-file" accept="image/*">
                  </div>
                </div>
                <div class="form-group-material">
                  <input id="update-name" type="text" name="name" required data-msg="Please enter your name" class="input-material"
                    value="<?= $people['name'] ?? '' ?>">
                  <label for="update-username" class="label-material">Name</label>
                </div>
                <div class="form-group-material">
                  <input id="update-phone" type="text" name="phone" data-msg="Please enter your phone number" class="input-material"
                    value="<?= $people['phone'] ?? '' ?>">
                  <label for="update-phone" class="label-material">Phone</label>
                </div>
                <div class="form-group-material">
                  <input id="update-address" type="text" name="address" data-msg="Please enter your address" class="input-material"
                    value="<?= $people['address'] ?? '' ?>">
                  <label for="update-address" class="label-material">Address</label>
                </div>
            </div>
            <div class="card-footer">
              <div class="text-right">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      
      <!-- Form-->
      <div class="col-lg-6">
        <div id="update-user-information" class="form card">
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">User configuration</h3>
          </div>
          
          <form class="form-validate" action="<?= base_url('user/update') ?>" method="post">
            <div class="card-body">
              <p>Basic user configuration</p>
                <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
                <div class="form-group-material">
                  <input id="update-email" type="email" name="email" required data-msg="Please enter a valid email address" class="input-material"
                    value="<?= $user['email'] ?? '' ?>" disabled="disabled">
                  <label for="update-email" class="label-material">Email Address</label>
                </div>
                <div class="form-group-material">
                  <input id="update-password" type="password" name="password" required data-msg="Please enter your password" class="input-material"
                    value="">
                  <label for="update-password" class="label-material">Password</label>
                </div>
            </div>
            <div class="card-footer">
              <div class="text-right">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>
  </div>
</section>

<?= $this->endSection() ?>


<?= $this->section('scripts') ?>
  
  <!-- Main File-->
  <script src="/public/admin/js/custom.js"></script>
  <script src="/public/admin/js/user.js"></script>

<?= $this->endSection() ?>
