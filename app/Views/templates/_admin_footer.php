<?php
  $session = \Config\Services::session();
  $appName = $session->get('appName');
?>

<!-- CSRF -->
<input type="hidden" id="csrf-hash-input" value="<?= csrf_hash() ?>" />
<!-- Modal -->
<div id="modal-info-box" class="offset-sm-8 col-sm-4"></div>
<!-- Modal -->
<div id="modal-content-box" class="page login-page modal fade" role="dialog">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <div id="modal_target" class="modal-dialog"></div>
</div>

<!-- Page Footer-->
<footer class="main-footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <p><?= $appName ?> &copy; <?= date('Y') ?></p>
      </div>
      <div class="col-sm-6 text-right">
        <p>Developed by <a href="#" class="external">Linker</a></p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </div>
    </div>
  </div>
</footer>