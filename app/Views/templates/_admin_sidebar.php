<?php
  $session = \Config\Services::session();
  $role = $session->get('role');
  $avatar = $session->get('avatar');
  $uri = uri_string();
?>

<!-- Side Navbar -->
<nav class="side-navbar">
  <!-- Sidebar Header-->
  <div class="sidebar-header d-flex align-items-center">
    <div class="avatar">
      <img src="<?= $avatar!=NULL || $avatar!='' ? '/static/avatars/'.$avatar : '/public/admin/img/avatar.png' ?>" alt="Avatar" class="img-fluid rounded-circle">
    </div>
    <div class="title">
      <h1 class="h4"><?= $session->get('name') ?></h1>
      <p>Web Designer</p>
    </div>
  </div>
  <!-- Sidebar Navidation Menus-->
  <span class="heading">Main</span>
  <ul class="list-unstyled">
    <?php if ($role == App\Helpers\Constants::ADMIN_ROLE): ?>
      <li class="<?= stripos($uri, 'admin') > 0 ? 'active' : '' ?>">
        <a href="<?= base_url('admin') ?>"> <i class="icon-home"></i>Home </a>
      </li>
    <?php endif; ?>
    <?php if (in_array($role, [App\Helpers\Constants::ADMIN_ROLE, App\Helpers\Constants::CLIENT_ROLE])): ?>
      <li class="<?= stripos($uri, 'user') > 0 ? 'active' : '' ?>">
        <a href="<?= base_url('user') ?>"> <i class="fa fa-cog"></i>Settings </a>
      </li>
    <?php endif; ?>
  </ul>
</nav>
