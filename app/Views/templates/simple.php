<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= isset($title) ? $title : 'Home' ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="/public/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="/public/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Google fonts - Roboto-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="/public/css/style.default.login.css" id="theme-stylesheet">
    <link rel="stylesheet" href="/public/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="/public/css/custom.css">
    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="/public/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="/public/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/public/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/public/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/public/img/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/public/img/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/public/img/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/public/img/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/public/img/apple-touch-icon-152x152.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div id="all">
      <!-- Top bar-->
      <div class="top-bar">
        <div class="container">
          <div class="row d-flex align-items-center">
            <div class="col-md-6 d-md-block d-none">
              <p>Contact us on +420 777 555 333 or hello@universal.com.</p>
            </div>
            <div class="col-md-6">
              <div class="d-flex justify-content-md-end justify-content-between">
                <ul class="list-inline contact-info d-block d-md-none">
                  <li class="list-inline-item"><a href="#"><i class="fa fa-phone"></i></a></li>
                  <li class="list-inline-item"><a href="#"><i class="fa fa-envelope"></i></a></li>
                </ul>
                <div class="login">
                  <a href="#" class="login-btn" onclick="get_signin_modal(event, 'signin');">
                    <i class="fa fa-sign-in"></i>
                    <span class="d-none d-md-inline-block">Sign In</span>
                  </a>
                  <a href="#" class="signup-btn" onclick="get_signin_modal(event, 'signup');">
                    <i class="fa fa-user"></i>
                    <span class="d-none d-md-inline-block">Sign Up</span>
                  </a>
                </div>
                <ul class="social-custom list-inline">
                  <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li class="list-inline-item"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li class="list-inline-item"><a href="#"><i class="fa fa-envelope"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Top bar end-->
      <!-- Navbar Start-->
      <header class="nav-holder make-sticky">
        <div id="navbar" role="navigation" class="navbar navbar-expand-lg">
          <div class="container"><a href="<?= base_url() ?>" class="navbar-brand home"><img src="/public/img/logo.png" alt="Universal logo" class="d-none d-md-inline-block"><img src="/public/img/logo-small.png" alt="Universal logo" class="d-inline-block d-md-none"><span class="sr-only">Universal - go to homepage</span></a>
            <button type="button" data-toggle="collapse" data-target="#navigation" class="navbar-toggler btn-template-outlined"><span class="sr-only">Toggle navigation</span><i class="fa fa-align-justify"></i></button>
            <div id="navigation" class="navbar-collapse collapse">
              <ul class="nav navbar-nav ml-auto">
                <!-- ========== Contact dropdown ==================-->
                <li class="nav-item dropdown"><a href="javascript: void(0)" data-toggle="dropdown" class="dropdown-toggle">Contact <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li class="dropdown-item"><a href="contact.html" class="nav-link">Contact option 1</a></li>
                    <li class="dropdown-item"><a href="contact2.html" class="nav-link">Contact option 2</a></li>
                    <li class="dropdown-item"><a href="contact3.html" class="nav-link">Contact option 3</a></li>
                  </ul>
                </li>
                <!-- ========== Contact dropdown end ==================-->
              </ul>
            </div>
            <div id="search" class="collapse clearfix">
              <form role="search" class="navbar-form">
                <div class="input-group">
                  <input type="text" placeholder="Search" class="form-control"><span class="input-group-btn">
                    <button type="submit" class="btn btn-template-main"><i class="fa fa-search"></i></button></span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </header>
      <!-- Navbar End-->
	  
      <?= $this->renderSection('content') ?>
      
      <!-- CSRF -->
      <input type="hidden" id="csrf-hash-input" value="<?= csrf_hash() ?>" />
      <!-- Modal -->
      <div id="modal-info-box" class="offset-sm-8 col-sm-4"></div>
      <!-- Modal -->
      <div id="modal-content-box" class="page login-page modal fade" role="dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div id="modal_target" class="modal-dialog"></div>
      </div>
      
      <!-- FOOTER -->
      <footer class="main-footer p-0">
        <div class="copyrights">
          <div class="container">
            <div class="row">
              <div class="col-lg-4 text-center-md">
                <p>&copy; 2020. Your company / name goes here</p>
              </div>
              <div class="col-lg-8 text-right text-center-md">
                <p>Template design by <a href="https://bootstrapious.com/snippets">Bootstrapious </a>&  <a href="https://fity.cz/">Fity</a></p>
                <!-- Please do not remove the backlink to us unless you purchase the Attribution-free License at https://bootstrapious.com/donate. Thank you. -->
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <!-- SCRIPTS -->
    <script type="text/javascript">
      function get_signin_modal(evt, action) {
        evt.preventDefault();

            $.ajax({
                type: 'POST', 
                url: '<?php base_url() ?>/get_'+action+'_modal',
          cache: false,
          headers: {
            'X-CSRF-TOKEN': '<?= csrf_hash() ?>'
          },
                success : function(data){ 
            let target = $('#modal_target');
            if(data){
              target.html(data);
              //This shows the modal
              $('#modal-content-box').modal();
              activateMaterialForm( target );
            }
          }
        });
      }
    </script>

    <!-- Javascript files-->
    <script src="/public/vendor/jquery/jquery.min.js"></script>
    <script src="/public/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="/public/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/public/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="/public/vendor/waypoints/lib/jquery.waypoints.min.js"> </script>
    <script src="/public/vendor/jquery.counterup/jquery.counterup.min.js"> </script>
    <script src="/public/vendor/jquery.scrollto/jquery.scrollTo.min.js"></script>
    <script src="/public/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="/public/js/custom.js"></script>
  </body>
</html>