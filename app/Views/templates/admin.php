<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= isset($title) ? $title : 'Admin' ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="/public/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="/public/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="/public/admin/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="/public/admin/css/style.green.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="/public/admin/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="/public/admin/img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>

    <div class="page">
      
      <?= $this->include('templates/_admin_navbar') ?>
      
      <div class="page-content d-flex align-items-stretch"> 

        <?= $this->include('templates/_admin_sidebar') ?>
        
        <div class="content-inner">
          
          <?= $this->renderSection('content') ?>

          <?= $this->include('templates/_admin_footer') ?>
        
        </div>
      </div>
    </div>

    <!-- CSRF -->
    <input type="hidden" id="csrf-hash-input" value="<?= csrf_hash() ?>" />

    <!-- JavaScript files-->
    <script src="/public/vendor/jquery/jquery.min.js"></script>
    <script src="/public/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="/public/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/public/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="/public/vendor/jquery.cookie/jquery.cookie.js"> </script>

    <?= $this->renderSection('scripts') ?>
  </body>
</html>