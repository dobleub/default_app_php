$(document).ready(function() {
	/**
	 * Global vars
	 */
	let csrf = $('#csrf-hash-input').val();

	/**
	 * Binding events
	 */
	$('#update-personal-information').each(function(idx, item) {
		activateMaterialForm( $(item), csrf, '/user' );
	});
	$('#update-user-information').each(function(idx, item) {
		activateMaterialForm( $(item), csrf, '/user' );
	});
	$('#update-avatar').change(function() {
		readURL(this);
	});

	/**
	 *	Read Url for preview images
	 */
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#preview-update-avatar').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]); // convert to base64 string
		}
	}

});