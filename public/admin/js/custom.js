$(document).ready(function() {

});

function toggleMenu() {
  var menuItems = document.getElementsByClassName('menu-item');
  for (var i = 0; i < menuItems.length; i++) {
    var menuItem = menuItems[i];
    menuItem.classList.toggle("hidden");
  }
}

function showModal(type, text) {
  let modal = $('#modal-info-box');

  modal.html(`<div class="alert alert-${type} alert-dismissible fade show" role="alert">
      <div class="text-info">${text}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div >`);
  
  setTimeout(function() {
    modal.find('button').eq(0).trigger('click');
  }, 1000 * 5);
}

function activateMaterialForm(form, csrfToken, redirectTo, modalBox, successCallback, progressCallback) {
  // ------------------------------------------------------- //
  // Universal Form Validation
  // ------------------------------------------------------ //
  form.find('.form-validate').each(function(idx, item) {
    $(item).validate({
      errorElement: "div",
      errorClass: 'is-invalid',
      validClass: 'is-valid',
      ignore: ':hidden:not(.summernote, .checkbox-template, .form-control-custom),.note-editable.card-block',
      errorPlacement: function (error, element) {
        // Add the `invalid-feedback` class to the error element
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.siblings("label"));
        } 
        else {
          error.insertAfter(element);
        }
      }
    });
  });

  form.find('.form-validate').submit(function(e) {
    e.preventDefault();
    e.stopPropagation();

    let tmpForm = $(this);
    let nErrors = tmpForm.find('input.is-invalid');
    let action = tmpForm.attr('action');
    let method = tmpForm.attr('method');
    
    if (nErrors.length === 0 && action && method) {
      let data = serializeToJson(tmpForm);
      let formData = getFormData(tmpForm);
      
      $.ajax({
        type: method,
        url: action,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        headers: {
          "X-Requested-With": "XMLHttpRequest",
          'X-CSRF-TOKEN': csrfToken
        },
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                if (typeof progressCallback == 'Function') {
                  myXhr.upload.addEventListener('progress', progressCallback, false);
                } else {
                  myXhr.upload.addEventListener('progress', progressSampleCallback, false);
                }
            }
            return myXhr;
        },
        complete: function(res) {
          let data = JSON.parse(res.responseText);
          if (data.status == 'ok') {
            
            if (modalBox) {
              modalBox.modal('hide');
            }

            if (redirectTo) {
              setTimeout(() => {
                  location.href = redirectTo;
              }, 100);
            }

            if (typeof successCallback == 'Function') {
              successCallback(data);
            }
          } else {
            showModal('danger', data.msg || 'Error');
          }
        },
        error: function(res) {
          let data = JSON.parse(res.responseText);
          showModal('danger', data.msg || data.message || 'Error');
        }
      });
    } 
  });

  // ------------------------------------------------------- //
  // Material Inputs
  // ------------------------------------------------------ //
  var materialInputs = form.find('input.input-material');
  // activate labels for prefilled values
  materialInputs.filter(function() { return $(this).val() !== ""; }).siblings('.label-material').addClass('active');
  // move label on focus
  materialInputs.on('focus', function () {
    $(this).siblings('.label-material').addClass('active');
  });
  // remove/keep label on blur
  materialInputs.on('blur', function () {
    $(this).siblings('.label-material').removeClass('active');

    if ($(this).val() !== '') {
      $(this).siblings('.label-material').addClass('active');
    } else {
      $(this).siblings('.label-material').removeClass('active');
    }
  });
}
function progressSampleCallback (event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    var progress_bar_id = "#progress-wrp";
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    // $(progress_bar_id + " .status").text(percent + "%");
};

function serializeToJson(form) {
  var o = {};
  var a = form.serializeArray();
  $.each(a, function() {
      if (o[this.name]) {
          if (!o[this.name].push) {
              o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
      } else {
          o[this.name] = this.value || '';
      }
  });
  return o;
}

function getFormData(form) {
  let formData = new FormData();

  form.find('input').each(function(idx, item) {
    let name = item.getAttribute('name');
    let type = item.getAttribute('type');

    if (type == 'file' && item.files['length'] > 0) {
      formData.append(name, item.files[0]);
    } else {
      formData.append(name, item.value);
    }
  });

  return formData;
}