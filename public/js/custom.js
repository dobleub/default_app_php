$(document).ready(function() {
  let csrf = $('#csrf-hash-input').val();

  $('.form').each(function(idx, item) {
    activateMaterialForm( $(item), csrf );
  });
});

function toggleMenu() {
  var menuItems = document.getElementsByClassName('menu-item');
  for (var i = 0; i < menuItems.length; i++) {
    var menuItem = menuItems[i];
    menuItem.classList.toggle("hidden");
  }
}

function showModal(type, text) {
  let modal = $('#modal-info-box');

  modal.html(`<div class="alert alert-${type} alert-dismissible fade show" role="alert">
      <div class="text-info">${text}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div >`);
  
  setTimeout(function() {
    modal.find('button').eq(0).trigger('click');
  }, 1000 * 5);
}

function activateMaterialForm(form, csrfToken, redirectTo, modalBox) {
  // ------------------------------------------------------- //
  // Universal Form Validation
  // ------------------------------------------------------ //
  form.find('.form-validate').each(function(idx, item) {
    $(item).validate({
      errorElement: "div",
      errorClass: 'is-invalid',
      validClass: 'is-valid',
      ignore: ':hidden:not(.summernote, .checkbox-template, .form-control-custom),.note-editable.card-block',
      errorPlacement: function (error, element) {
        // Add the `invalid-feedback` class to the error element
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.siblings("label"));
        } 
        else {
          error.insertAfter(element);
        }
      }
    });
  });

  form.find('.form-validate').submit(function(e) {
    e.preventDefault();
    e.stopPropagation();

    let tmpForm = $(this);
    let nErrors = tmpForm.find('input.is-invalid');
    let action = tmpForm.attr('action');
    let method = tmpForm.attr('method');
    
    if (nErrors.length === 0 && action && method) {
      let data = serializeToJson(tmpForm);

      $.ajax({
        type: method,
        url: action,
        data: tmpForm.serialize(),
        headers: {
          "X-Requested-With": "XMLHttpRequest",
          'X-CSRF-TOKEN': csrfToken
        },
        complete: function(res) {
          let data = JSON.parse(res.responseText);
          if (data.status == 'ok') {
            tmpForm[0].reset();
            
            if (modalBox) {
              modalBox.modal('hide');
            }

            if (redirectTo) {
              setTimeout(() => {
                  location.href = redirectTo;
              }, 100);
            }
          } else {
            showModal('danger', data.msg || 'Error');
          }
        },
        error: function(res) {
          let data = JSON.parse(res.responseText);
          showModal('danger', data.msg || data.message || 'Error');
        }
      });
    } 
  });

  // ------------------------------------------------------- //
  // Material Inputs
  // ------------------------------------------------------ //
  var materialInputs = form.find('input.input-material');
  // activate labels for prefilled values
  materialInputs.filter(function() { return $(this).val() !== ""; }).siblings('.label-material').addClass('active');
  // move label on focus
  materialInputs.on('focus', function () {
    $(this).siblings('.label-material').addClass('active');
  });
  // remove/keep label on blur
  materialInputs.on('blur', function () {
    $(this).siblings('.label-material').removeClass('active');

    if ($(this).val() !== '') {
      $(this).siblings('.label-material').addClass('active');
    } else {
      $(this).siblings('.label-material').removeClass('active');
    }
  });
}

function serializeToJson(form) {
  var o = {};
  var a = form.serializeArray();
  $.each(a, function() {
      if (o[this.name]) {
          if (!o[this.name].push) {
              o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
      } else {
          o[this.name] = this.value || '';
      }
  });
  return o;
}